<?php

return array(
    'user'      => new User(),
    'teacher'   => new Teacher(),
    'student'   => new Student(),
    'race'      => new Race(),
    'proyect'   => new ProyectEstudent(),
    'category'  => new Category(),
    'location'  => new Location(),
    'visit'     => new Visit()
);