<?php

include './init.php';
include './controller/connection/Connection.php';
include './controller/teacher/Teacher.php';
include './controller/student/Student.php';
include './controller/student/StudentRace.php';
include './controller/user/User.php';
include './controller/proyect/Proyect.php';
include './controller/proyect/ProyectEstudent.php';
include './controller/category/Category.php';
include './controller/location/Country.php';
include './controller/location/Location.php';
include './controller/visit/Visit.php';

$instance = include 'instances.php';

$instance['visit']->newVisit();

if(isset($_SESSION['page']) && isset($_GET['page'])){
    $_SESSION['page'] = $_GET['page'];
}

$page = isset($_SESSION['page']) ? $_SESSION['page'] : 'home';

$pages = getFiles('pages');
$partial = getFiles('partial');
$post = getFiles('request/post');
$get = getFiles('request/get');

$css = getFiles('css');
$js = getFiles('js');
$img = getFiles('assets/img');
$demo = getFiles('assets/demo');

include './pages/index.php';