<?php
    $categories = $instance['category']->getCategories();
?>
<?php if($instance['user']->Admin()):?>
<h1 class="mt-4">Tipos de Proyectos</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Tipos de Proyectos</li>
</ol>

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Tipos de Proyectos
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <caption>
                    <button data-toggle="modal" data-target="#addCategory" class="btn btn-outline-primary" title="Agregar" data-toggle="modal" data-target="#addProyectType">
                        <i class="fas fa-plus"></i>
                    </button>
                </caption>
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Nombre</th>
                        <th>Opciones</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php foreach($categories as $key => $category): ?>
                    <tr>
                        <td><?=$category['nombre']?></td>
                        <td>
                            <a href="#" class="btn btn-outline-primary" title="Actualizar" data-toggle="modal" data-target="#upCategory<?=$key?>"><i class="fas fa-user-edit"></i></a>
                            <!-- Actualizar Categoría -->
                            <div class="modal fade" id="upCategory<?=$key?>" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Actualizar Categoría</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="<?=$post['upCategory']?>?page=<?=$page?>" method="POST">
                                        <input type="hidden" name="idCategory" value="<?=$key?>" />
                                        <div class="modal-body">
                                            <div class="form-row">
                                                <div class="col">
                                                    <label for="txtUpName<?=$key?>">Nombre de Categoría<i style="color: red;">*</i></label>
                                                    <input type="text" name="txtName" value="<?=$category['nombre']?>" id="txtUpName<?=$key?>" class="form-control" placeholder="Categoría" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-outline-primary">Actualizar</button>
                                        </div>
                                    </form>
                                  </div>
                                </div>
                            </div>
                            <a href="#" urlDel="<?=$get['delCategory']?>?page=<?=$page?>&id=<?=$key?>" onclick="onClickDelete(this)" class="btn btn-outline-danger" title="Eliminar"><i class="fas fa-trash-alt"></i></a>
                            <a href="#" class="btn btn-outline-secondary" title="Ver" data-toggle="modal" data-target="#showCategory<?=$key?>"><i class="fas fa-eye"></i></a>
                            <!-- Modal Mostrar Categoría -->
                            <div class="modal fade" id="showCategory<?=$key?>" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Visualizando Categoría</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-row">
                                            <div class="col">
                                                <label for="txtRaceName<?=$key?>">Nombre de Categoria</label>
                                                <input type="text" value="<?=$category['nombre']?>" id="txtRaceName<?=$key?>" class="form-control" placeholder="Nombre" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php else: ?>
    <script>
        window.location = "./?page=home";
    </script>
<?php endif;?>
<!-- Nueva Categoría -->
<div class="modal fade" id="addCategory" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nueva Categoría</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
          <form action="<?=$post['addCategory']?>?page=<?=$page?>" method="POST">
            <div class="modal-body">
                <div class="form-row">
                    <div class="col">
                        <label for="txtAddNameRace">Nombre de Categoría</label>
                        <input type="text" name="txtName" id="txtAddNameRace" class="form-control" placeholder="Nombre de Categoría" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-outline-primary">Guardar</button>
            </div>
        </form>
      </div>
    </div>
</div>

<?php include $partial['messages']; ?>