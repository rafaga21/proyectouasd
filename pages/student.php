<?php
    $students = $instance['student']->getStudents();
    $races = $instance['race']->getRaces();
?>

<h1 class="mt-4">Estudiantes</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Personas / Estudiantes</li>
</ol>

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Estudiantes
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <caption>
                    <button class="btn btn-outline-primary" data-toggle="modal" data-target="#addStudent" title="Agregar Usuario">
                        <i class="fas fa-plus"></i>
                    </button>
                </caption>
                <thead>
                    <tr>
                        <th>Imagen</th>
                        <th>Matrícula</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Carrera</th>
                        <th>En Proyecto</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Imagen</th>
                        <th>Matrícula</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Carrera</th>
                        <th>En Proyecto</th>
                        <th>Opciones</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php foreach($students as $key => $student):?>
                    <tr>
                        <td>
                            <center>
                                <img src="<?=$img['student']?>" width="40" />
                            </center>
                        </td>
                        <td><?=$student['matricula']?></td>
                        <td><?=$student['nombre']?></td>
                        <td><?=$student['apellido']?></td>
                        <td><?=$student['carrera']?></td>
                        <td>
                            <center>
                                <?php if($instance['student']->isStudentInProyect($key)): ?>
                                    <img src="<?=$img['check']?>" width="40" />
                                <?php else: ?>
                                    <img src="<?=$img['x']?>" width="40" />
                                <?php endif; ?>
                            </center>
                        </td>
                        <td>
                            <a href="#" data-toggle="modal" data-target="#addStudent<?=$key?>" class="btn btn-outline-primary" title="Actualizar"><i class="fas fa-user-edit"></i></a>
                            <!-- Actualizar Estudiante -->
                            <div class="modal fade" id="addStudent<?=$key?>" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Actualizar Estudiante</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                      <form action="<?=$post['upStudent']?>?page=<?=$page?>" method="POST">
                                          <input type="hidden" name="idStudent" value="<?=$key?>" />
                                        <div class="modal-body">
                                            <div class="form-row">
                                                <div class="col">
                                                    <label for="txtUpName<?=$key?>">Nombre <i style="color: red;">*</i></label>
                                                    <input type="text" name="txtName" value="<?=$student['nombre']?>" id="txtUpName<?=$key?>" class="form-control" placeholder="Nombre" required>
                                                </div>
                                                <div class="col">
                                                    <label for="txtAddLastName<?=$key?>">Apellido <i style="color: red;">*</i></label>
                                                    <input type="text" name="txtLastName" value="<?=$student['apellido']?>" id="txtAddLastName<?=$key?>" class="form-control" placeholder="Apellido" required>
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="form-group">
                                                <label for="txtUpMatricula<?=$key?>">Matricula <i style="color: red;">*</i></label>
                                                <input type="text" name="txtMatricula" id="txtUpMatricula<?=$key?>" value="<?=$student['matricula']?>" class="form-control" placeholder="Usuario" required>
                                                <small id="emailHelp" class="form-text text-muted">nombre de usuario.</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtAddPass">Carreras <i style="color: red;">*</i></label>
                                                <select class="form-control" name="txtCarrera">
                                                    <?php foreach($races as $keyRaces => $race): ?>
                                                        <?php if($student['carrera'] == $race['nombre']): ?>
                                                            <option value="<?=$keyRaces?>" selected><?=$race['nombre']?></option>
                                                        <?php else: ?>
                                                            <option value="<?=$keyRaces?>"><?=$race['nombre']?></option>
                                                        <?php endif;?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-outline-primary">Actualizar</button>
                                        </div>
                                    </form>
                                  </div>
                                </div>
                            </div>
                            
                            <a href="#" urlDel="<?=$get['delStudent']?>?page=<?=$page?>&id=<?=$key?>" onclick="onClickDelete(this)" class="btn btn-outline-danger" title="Eliminar"><i class="fas fa-trash-alt"></i></a>
                            <a href="#" class="btn btn-outline-secondary" data-toggle="modal" data-target="#showtudent<?=$key?>" title="Ver"><i class="fas fa-eye"></i></a>
                            <!-- Mostrar Estudiante -->
                            <div class="modal fade" id="showtudent<?=$key?>" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Visualizando Estudiante</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                      <form action="<?=$post['upStudent']?>?page=<?=$page?>" method="POST">
                                          <input type="hidden" name="idStudent" value="<?=$key?>" />
                                        <div class="modal-body">
                                            <div class="form-row">
                                                <div class="col">
                                                    <label for="txtUpName">Nombre</label>
                                                    <input type="text" value="<?=$student['nombre']?>" id="txtUpName" class="form-control" readonly>
                                                </div>
                                                <div class="col">
                                                    <label for="txtAddLastName">Apellido</label>
                                                    <input type="text" value="<?=$student['apellido']?>" id="txtAddLastName" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="form-group">
                                                <label for="txtUpMatricula">Matricula</label>
                                                <input type="text" id="txtUpMatricula" value="<?=$student['matricula']?>" class="form-control" readonly>
                                                <small id="emailHelp" class="form-text text-muted">nombre de usuario.</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtAddPass">Carreras</label>
                                                <input type="text" id="txtUpMatricula" value="<?=$student['carrera']?>" class="form-control" readonly>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </form>
                                  </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Nuevo Estudiante -->
<div class="modal fade" id="addStudent" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nuevo Estudiante</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
          <form action="<?=$post['addStudent']?>?page=<?=$page?>" method="POST">
            <div class="modal-body">
                <div class="form-row">
                    <div class="col">
                        <label for="txtaddname">Nombre</label>
                        <input type="text" name="txtName" id="txtaddname" class="form-control" placeholder="Nombre" required>
                    </div>
                    <div class="col">
                        <label for="txtAddLastName">Apellido</label>
                        <input type="text" name="txtLastName" id="txtAddLastName" class="form-control" placeholder="Apellido" required>
                    </div>
                </div>
                <br/>
                <div class="form-group">
                    <label for="txtAddUser">Matricula</label>
                    <input type="text" name="txtMatricula" id="txtAddUser" class="form-control" placeholder="Matricula de estudiante" required>
                </div>
                <div class="form-group">
                    <label for="txtAddPass">Carreras:</label>
                    <select class="form-control" name="txtCarrera">
                        <?php foreach($races as $key => $race): ?>
                        <option value="<?=$key?>"><?=$race['nombre']?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-outline-primary">Guardar</button>
            </div>
        </form>
      </div>
    </div>
</div>

<?php include $partial['messages'];?>