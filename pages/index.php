<script>
    function onClickDelete(value) {
        Swal.fire({
            title: '¿Realmente Quieres Eliminar Este Registro?',
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonText: 'Cancelar',
            denyButtonText: 'Eliminar',
        }).then((result) => {
            if (result.isDenied) {
              window.location = $(value).attr('urlDel');
            }
        })
    }
    history.replaceState(null, '', './');// cambia la url de la barra de busqueda.
</script>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>.: SITA :.</title>
        <link href="<?=$css['styles']?>" rel="stylesheet" />
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <?php include $partial['library']; ?>
    </head>
    <body class="sb-nav-fixed">
        <?php if($instance['user']->Auth()): ?>
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="./"><img src="<?=$img['sita']?>" width="40" /> SITA</a>
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group"></div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i> <?=$instance['user']->getUserName()?></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <?php if($instance['user']->Admin()): ?>
                            <a class="dropdown-item" href="./?page=visit">
                                <i class="fas fa-eye"></i> Visitas
                            </a>
                        <?php endif;?>
                        <a class="dropdown-item" href="<?=$get['logout']?>"><i class="fas fa-sign-out-alt"></i> Cerrar Sesión</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Principal</div>
                            <a class="nav-link" href="./?page=home">
                                <div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                                Inicio
                            </a>
                            <div class="sb-sidenav-menu-heading">Personas</div>
                            <a class="nav-link collapsed" href="./?page=people" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                                Personas
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="./?page=teacher"><div class="sb-nav-link-icon"><i class="fas fa-chalkboard-teacher"></i></div>Maestros</a>
                                    <a class="nav-link" href="./?page=student"><div class="sb-nav-link-icon"><i class="fas fa-user-graduate"></i></div>Estudiantes</a>
                                </nav>
                            </div>
                            <?php if($instance['user']->Admin()):?>
                            <a class="nav-link" href="./?page=user">
                                <div class="sb-nav-link-icon"><i class="fas fa-address-book"></i></div>
                                Usuarios
                            </a>
                            <div class="sb-sidenav-menu-heading">Universidad</div>
                            <a class="nav-link" href="./?page=race">
                                <div class="sb-nav-link-icon"><i class="fas fa-graduation-cap"></i></div>
                                Carreras
                            </a>
                            <a class="nav-link" href="./?page=location">
                                <div class="sb-nav-link-icon"><i class="fas fa-map-marker-alt"></i></div>
                                Ubicaciones
                            </a>
                            <?php endif;?>
                            <div class="sb-sidenav-menu-heading">Proyectos</div>
                            <?php if($instance['user']->Admin()):?>
                            <a class="nav-link" href="./?page=proyecttype">
                                <div class="sb-nav-link-icon"><i class="fas fa-bookmark"></i></div>
                                Tipos de Proyectos
                            </a>
                            <?php endif;?>
                            <a class="nav-link" href="./?page=proyect">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-reader"></i></div>
                                Proyectos
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logueado Como: <i style="color: white;"><?= $instance['user']->Admin()?'Administrador' : 'Usuario'?></i></div>
                        <?=$instance['user']->getName()?>
                    </div>
                </nav>
            </div>
            
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <?php if(isset($pages[$page])):?>
                            <?php include $pages[$page];?>
                        <?php else: ?>
                            <?php include $pages['home'];?>
                        <?php endif;?>
                    </div>
                </main>
                <?php include $partial['footer']; ?>
            </div>
        </div>
        <?php else: ?>
            <?php include $pages['login']; ?>
        <?php endif; ?>
        
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <?php foreach ($js as $key => $value): ?>
        <script src="<?=$value?>"></script>
        <?php endforeach;?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="<?=$demo['datatables-demo']?>"></script>
    </body>
</html>
