<?php
    $visits = $instance['visit']->getAllVisits();
?>
<?php if($instance['user']->Admin()): ?>
<h1 class="mt-4">Visitas</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Visitas</li>
</ol>

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Visitas
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>IP DE USUARIO</th>
                        <th>Nombre</th>
                        <th>Usuario</th>
                        <th>VISITAS</th>
                        <th>&Uacute;ltimo Acceso</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Total de IP</th>
                        <th>Nombres</th>
                        <th>Usuario</th>
                        <th>Total de Visitas</th>
                        <th>&Uacute;ltimo Acceso</th>
                    </tr>
                    <tr style="background-color: #239DDF;color: white;">
                        <th><?= count($visits)?></th>
                        <th></th>
                        <th></th>
                        <th><?=$instance['visit']->getTotalVisits()?></th>
                        <th></th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php foreach($visits as $visit): ?>
                    <tr>
                        <td><img src="<?=$img['ip']?>" width="25" /> <?=$visit['ipVisita']?></td>
                        <td><?=$visit['usuario']?></td>
                        <td><?=$visit['nombreUsuario']?></td>
                        <td style="color:green;font-size: 30px;font-weight: bold;text-align: center;"><?=$visit['cantidadVisita']?></td>
                        <td style="color: red;"><?=$visit['fecha']?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php else: ?>
    <script>
        window.location = "./?page=home";
    </script>
<?php endif;?>