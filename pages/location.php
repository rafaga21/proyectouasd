<?php
    $locations = $instance['location']->getLocations();
    $countries = $instance['location']->getCountries();
?>

<h1 class="mt-4">Ubicación</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Ubicación</li>
</ol>

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Ubicación
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <caption>
                    <button data-toggle="modal" data-target="#addLocation" class="btn btn-outline-primary" title="Agregar" data-toggle="modal" data-target="#addProyectType">
                        <i class="fas fa-plus"></i>
                    </button>
                </caption>
                <thead>
                    <tr>
                        <th>Lugar</th>
                        <th>Centro</th>
                        <th>Facultad</th>
                        <th>Escuela</th>
                        <th>Ciudad</th>
                        <th>País</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Lugar</th>
                        <th>Centro</th>
                        <th>Facultad</th>
                        <th>Escuela</th>
                        <th>Ciudad</th>
                        <th>País</th>
                        <th>Opciones</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php foreach($locations as $key => $location): ?>
                    <tr>
                        <td><?=$location['lugar']?></td>
                        <td><?=$location['centro']?></td>
                        <td><?=$location['facultad']?></td>
                        <td><?=$location['escuela']?></td>
                        <td><?=$location['ciudad']?></td>
                        <td><?=$location['pais']?></td>
                        <td>
                            <a href="#" class="btn btn-outline-primary" title="Actualizar" data-toggle="modal" data-target="#upLocation<?=$key?>"><i class="fas fa-user-edit"></i></a>
                            <!-- Actualizar Ubicación -->
                            <div class="modal fade" id="upLocation<?=$key?>" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Actualizar Ubicación</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                      <form action="<?=$post['upLocation']?>?page=<?=$page?>&id=<?=$key?>" method="POST">
                                        <div class="modal-body">
                                            <div class="form-row">
                                                <div class="col">
                                                    <label for="UpPlace<?=$key?>">Lugar</label>
                                                    <input type="text" value="<?=$location['lugar']?>" name="txtPlace" id="UpPlace<?=$key?>" class="form-control" placeholder="Nombre del Lugar" required>
                                                </div>
                                                <div class="col">
                                                    <label for="UpCenter<?=$key?>">Centro</label>
                                                    <input type="text" value="<?=$location['centro']?>" name="txtCenter" id="UpCenter<?=$key?>" class="form-control" placeholder="Nombre del Centro" required>
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="form-group">
                                                <label for="UpFaculty">Facultad</label>
                                                <input type="text" value="<?=$location['facultad']?>" name="txtFaculty" id="UpFaculty<?=$key?>" class="form-control" placeholder="Nombre de Facultad" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="UpSchool<?=$key?>">Escuela</label>
                                                <input type="text" value="<?=$location['escuela']?>" name="txtSchool" id="UpSchool<?=$key?>" class="form-control" placeholder="Nombre de la Escuela" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="UpCity<?=$key?>">Ciudad</label>
                                                <input type="text" value="<?=$location['ciudad']?>" name="txtCity" id="UpCity<?=$key?>" class="form-control" placeholder="Nombre de la Ciudad" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="">País</label>
                                                <select class="form-control" name="txtCountry">
                                                    <?php foreach($countries as $keyCountry => $country): ?>
                                                        <?php if($keyCountry == $location['idPais']): ?>
                                                            <option value="<?=$keyCountry?>" selected><?=$country['nombre']?></option>
                                                        <?php else: ?>
                                                            <option value="<?=$keyCountry?>" selected><?=$country['nombre']?></option>
                                                        <?php endif;?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-outline-primary">Actualizar</button>
                                        </div>
                                    </form>
                                  </div>
                                </div>
                            </div>
                            
                            <a href="#" urlDel="<?=$get['delLocation']?>?page=<?=$page?>&id=<?=$key?>" onclick="onClickDelete(this)" class="btn btn-outline-danger" title="Eliminar"><i class="fas fa-trash-alt"></i></a>
                            <a href="#" class="btn btn-outline-secondary" title="Ver" data-toggle="modal" data-target="#showLocation<?=$key?>"><i class="fas fa-eye"></i></a>
                            <!-- Mostrar Ubicación -->
                            <div class="modal fade" id="showLocation<?=$key?>" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Visualizando Ubicación</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-row">
                                            <div class="col">
                                                <label for="">Lugar</label>
                                                <input type="text" value="<?=$location['lugar']?>" class="form-control" readonly>
                                            </div>
                                            <div class="col">
                                                <label for="">Centro</label>
                                                <input type="text" value="<?=$location['centro']?>" class="form-control" readonly>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="form-group">
                                            <label for="">Facultad</label>
                                            <input type="text" value="<?=$location['facultad']?>" class="form-control" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Escuela</label>
                                            <input type="text" value="<?=$location['escuela']?>" class="form-control" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Ciudad</label>
                                            <input type="text" value="<?=$location['ciudad']?>" class="form-control" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="">País</label>
                                            <input type="text" value="<?=$location['pais']?>" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-outline-primary">Guardar</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Nueva Ubicación -->
<div class="modal fade" id="addLocation" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nueva Ubicación</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
          <form action="<?=$post['addLocation']?>?page=<?=$page?>" method="POST">
            <div class="modal-body">
                <div class="form-row">
                    <div class="col">
                        <label for="AddPlace">Lugar</label>
                        <input type="text" name="txtPlace" id="AddPlace" class="form-control" placeholder="Nombre del Lugar" required>
                    </div>
                    <div class="col">
                        <label for="AddCenter">Centro</label>
                        <input type="text" name="txtCenter" id="AddCenter" class="form-control" placeholder="Nombre del Centro" required>
                    </div>
                </div>
                <br/>
                <div class="form-group">
                    <label for="AddFaculty">Facultad</label>
                    <input type="text" name="txtFaculty" id="AddFaculty" class="form-control" placeholder="Nombre de Facultad" required>
                </div>
                <div class="form-group">
                    <label for="AddSchool">Escuela</label>
                    <input type="text" name="txtSchool" id="AddSchool" class="form-control" placeholder="Nombre de la Escuela" required>
                </div>
                <div class="form-group">
                    <label for="AddCity">Ciudad</label>
                    <input type="text" name="txtCity" id="AddCity" class="form-control" placeholder="Nombre de la Ciudad" required>
                </div>
                <div class="form-group">
                    <label for="">País</label>
                    <select class="form-control" name="txtCountry">
                        <?php foreach($countries as $key => $country): ?>
                        <option value="<?=$key?>"><?=$country['nombre']?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-outline-primary">Guardar</button>
            </div>
        </form>
      </div>
    </div>
</div>

<?php include $partial['messages'];?>