<?php
    $teachers = $instance['teacher']->getTeachers();
?>

<h1 class="mt-4">Maestros</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Personas / Maestros</li>
</ol>

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Maestros
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <caption>
                    <button class="btn btn-outline-primary" data-toggle="modal" data-target="#addTeacher" title="Agregar Usuario">
                        <i class="fas fa-plus"></i>
                    </button>
                </caption>
                <thead>
                    <tr>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Opciones</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php foreach($teachers as $key => $teacher): ?>
                    <tr>
                        <td>
                            <center>
                                <img src="<?=$img['teacher']?>" width="40" />
                            </center>
                        </td>
                        <td><?=$teacher['nombre']?></td>
                        <td><?=$teacher['apellido']?></td>
                        <td>
                            <a href="#" teacherId="<?=$key?>" teacherName="<?=$teacher['nombre']?>" teacherLastName="<?=$teacher['apellido']?>" onclick="loadDataTeacher(this)" data-toggle="modal" data-target="#upTeacher" class="btn btn-outline-primary" title="Actualizar"><i class="fas fa-user-edit"></i></a>
                            <a href="#" urlDel="<?=$get['delTeacher']?>?page=<?=$page?>&id=<?=$key?>" onclick="onClickDelete(this)" class="btn btn-outline-danger" title="Eliminar"><i class="fas fa-trash-alt"></i></a>
                            <a href="#" onclick="showDataTeacher(this)" teacherName="<?=$teacher['nombre']?>" teacherLastName="<?=$teacher['apellido']?>" data-toggle="modal" data-target="#showTeacher" class="btn btn-outline-secondary" title="Ver"><i class="fas fa-eye"></i></a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal nuevo maestro -->
<div class="modal fade" id="addTeacher" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nuevo Profesor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
          <form action="<?=$post['addTeacher']?>?page=<?=$page?>" method="POST">
            <div class="modal-body">
                <div class="form-row">
                    <div class="col">
                        <label for="txtaddname">Nombre</label>
                        <input type="text" name="txtName" id="txtaddname" class="form-control" placeholder="Nombre" required>
                    </div>
                    <div class="col">
                        <label for="txtAddLastName">Apellido</label>
                        <input type="text" name="txtLastName" id="txtAddLastName" class="form-control" placeholder="Apellido" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-outline-primary">Guardar</button>
            </div>
        </form>
      </div>
    </div>
</div>

<!-- Actualizar Usuario -->
<div class="modal fade" id="upTeacher" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Actualizar Profesor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="<?=$post['upTeacher']?>?page=<?=$page?>" method="POST">
            <input type="hidden" name="idTeacher" id="idTeacher" />
            <div class="modal-body">
                <div class="form-row">
                    <div class="col">
                        <label for="txtUpName">Nombre <i style="color: red;">*</i></label>
                        <input type="text" name="txtName" id="txtUpName" class="form-control" placeholder="Nombre" required>
                    </div>
                    <div class="col">
                        <label for="txtUpLastName">Apellido <i style="color: red;">*</i></label>
                        <input type="text" name="txtLastName" id="txtUpLastName" class="form-control" placeholder="Apellido" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-outline-primary">Actualizar</button>
            </div>
        </form>
      </div>
    </div>
</div>

<!-- Mostrar Profesor -->
<div class="modal fade" id="showTeacher" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Maestr@s</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="col">
                <label for="txtNameTeacher">Nombre</label>
                <input type="text" id="txtNameTeacher" class="form-control" readonly>
            </div>
            <div class="col">
                <label for="txtLastNameTeacher">Apellido</label>
                <input type="text" id="txtLastNameTeacher" class="form-control" readonly>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
</div>


<?php include $partial['messages']; ?>