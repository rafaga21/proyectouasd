<div id="layoutAuthentication">
    <div id="layoutAuthentication_content">
        <main>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="card shadow-lg border-1 rounded-lg mt-5">
                            <div class="card-header"><h3 class="text-center font-weight-light my-4">Login</h3></div>
                            <div class="card-body">
                                <form action="<?=$post['access']?>" method="post">
                                    <div class="form-group">
                                        <label class="small mb-1" for="txtUser">Usuario</label>
                                        <input name="username" class="form-control py-4" id="txtUser" type="text" placeholder="Ingresar Usuario" required/>
                                    </div>
                                    <div class="form-group">
                                        <label class="small mb-1" for="txtPass">Contraseña</label>
                                        <input name="password" class="form-control py-4" id="txtPass" type="password" placeholder="Ingresar Contraseña" required/>
                                    </div>
                                    <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                        <button type="submit" class="btn btn-primary">Acceder</button>
                                    </div>
                                    <?php if(isset($_GET['err'])): ?>
                                    <br />
                                    <div class="alert alert-danger" role="alert">Usuario y/o Clave Inválidos.</div>
                                    <?php endif; ?>
                                </form>
                            </div>
                            <div class="card-footer text-center">
                                <div class="small">Creado por [RMB]</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <div id="layoutAuthentication_footer">
        <?php include $partial['footer']; ?>
    </div>
</div>
