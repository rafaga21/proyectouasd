<?php
    $students = $instance['student']->getStudents();
    $teachers = $instance['teacher']->getTeachers();
    $users = $instance['user']->getUsers();
    $gProyects = $instance['proyect']->getProyectGroups();
    $typeProyects = $instance['category']->getCategories();
    $races = $instance['race']->getRaces();
    $location = $instance['location']->getLocations();
    $myVisits = $instance['visit']->getVisits();
    $allVisits = $instance['visit']->getTotalVisits();
?>

<h1 class="mt-4">Inicio</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">inicio</li>
</ol>

<div style="height: auto; width: 100%;" class="card mb-4">
    <div class="card-header">
        Actividades
    </div>
    <div class="card-body">
        <canvas id="SOMEID" height="70"></canvas>
    </div>
</div>
<br/>
 
<div class='row'>
    <div class="mb-4">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Estudiantes</h5>
                <h6 class="card-subtitle mb-2 text-muted">Registro de Estudiantes</h6>
                <p class="card-text">Actualmente hay <?= count($students)?> estudiantes registrados.</p>
                <a href="./?page=student" class="card-link btn btn-outline-info">ver</a>
            </div>
        </div>
    </div>
    <div class="mb-4">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Maestros</h5>
              <h6 class="card-subtitle mb-2 text-muted">Registro de Maestros</h6>
              <p class="card-text">Actualmente hay <?= count($teachers)?> maestros registrados.</p>
              <a href="./?page=teacher" class="card-link btn btn-outline-info">ver</a>
            </div>
        </div>
    </div>
    <div class="mb-4">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Usuarios</h5>
              <h6 class="card-subtitle mb-2 text-muted">Registro de Usuarios</h6>
              <p class="card-text">Actualmente hay <?= count($users)?> usuarios registrados.</p>
              <a href="<?=$instance['user']->Admin()?'./?page=user':'#'?>" class="card-link btn btn-outline-info">ver</a>
            </div>
        </div>
    </div>
    <div class="mb-4">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Proyectos en Grupos</h5>
              <h6 class="card-subtitle mb-2 text-muted">Registro de Proyectos</h6>
              <p class="card-text">Actualmente hay <?= count($gProyects)?> proyectos registrados.</p>
              <a href="./?page=proyect" class="card-link btn btn-outline-info">ver</a>
            </div>
        </div>
    </div>
    <div class="mb-4">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Tipos de Proyectos</h5>
              <h6 class="card-subtitle mb-2 text-muted">Registro de Tipos</h6>
              <p class="card-text">Actualmente hay <?= count($typeProyects)?> tipos de proyectos registrados.</p>
              <a href="<?=$instance['user']->Admin()?'./?page=proyecttype':'#'?>" class="card-link btn btn-outline-info">ver</a>
            </div>
        </div>
    </div>
    <div class="mb-4">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Carreras</h5>
              <h6 class="card-subtitle mb-2 text-muted">Registro de Carreras</h6>
              <p class="card-text">Actualmente hay <?= count($races)?> carreras registradas.</p>
              <a href="./?page=race" class="card-link btn btn-outline-info">ver</a>
            </div>
        </div>
    </div>
    <div class="mb-4">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Ubicaciones</h5>
              <h6 class="card-subtitle mb-2 text-muted">Registro de Ubicaciones</h6>
              <p class="card-text">Actualmente hay <?= count($location)?> ubicaciones registradas.</p>
              <a href="./?page=location" class="card-link btn btn-outline-info">ver</a>
            </div>
        </div>
    </div>
    <div class="mb-4">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Visitas</h5>
              <h6 class="card-subtitle mb-2 text-muted">Registro de Visitas</h6>
              <p class="card-text">Visitaste en total <?= $myVisits?> veces este sitio web.</p>
              <a href="<?=$instance['user']->Admin()?'./?page=visit':'#'?>" class="card-link btn btn-outline-info">ver</a>
            </div>
        </div>
    </div>
    <div class="mb-4">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Total de Visitas</h5>
              <h6 class="card-subtitle mb-2 text-muted">Registro de Visitas Totales</h6>
              <p class="card-text">El total de Visitas es <?= $allVisits?> de todos los usuarios registrados.</p>
              <a href="<?=$instance['user']->Admin()?'./?page=visit':'#'?>" class="card-link btn btn-outline-info">ver</a>
            </div>
        </div>
    </div>
</div>

<script>
    const MULTILINEONEFILL = document.getElementById("SOMEID");
        //console.log(MULTILINEONEFILL);
        let MultiLineOneFill = new Chart(MULTILINEONEFILL, {
        type: 'bar',
        data: {
            labels: ["Estudiantes", "Maestros", "Usuarios", "Proyectos", "Tipos de Proyectos", "Carreras", "Ubicaciones"],
            datasets: [
                {
                    label: "Registros",
                    fill: true,
                    lineTension: 0.3,
                    backgroundColor: [
                        "rgba(35, 170, 243, 0.4)",
                        "rgba(35, 243, 120,0.4)",
                        "rgba(190, 205, 40,0.4)",
                        "rgba(205, 66, 22,0.4)",
                        "rgba(94, 34, 222,0.4)",
                        "rgba(11, 21, 88,0.4)",
                        "rgba(193, 30, 131,0.4)",
                    ],
                    borderColor: [
                        "rgba(35, 170, 243, 0.4)",
                        "rgba(35, 243, 120,0.4)",
                        "rgba(190, 205, 40,0.4)",
                        "rgba(205, 66, 22,0.4)",
                        "rgba(94, 34, 222,0.4)",
                        "rgba(11, 21, 88,0.4)",
                        "rgba(193, 30, 131,0.4)",
                    ],
                    data: [
                        "<?=count($students)?>",
                        "<?=count($teachers)?>",
                        "<?=count($users)?>",
                        "<?=count($gProyects)?>",
                        "<?=count($typeProyects)?>",
                        "<?=count($races)?>",
                        "<?=count($location)?>",
                    ]
                }
            ]
        }
    });
</script>