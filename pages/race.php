<?php
    $races = $instance['race']->getRaces();
?>

<h1 class="mt-4">Tipos de Carreras</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Carreras</li>
</ol>

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Tipos de Carreras
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <caption>
                    <button class="btn btn-outline-primary" title="Agregar" data-toggle="modal" data-target="#addRace">
                        <i class="fas fa-plus"></i>
                    </button>
                </caption>
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Nombre</th>
                        <th>Opciones</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php foreach($races as $key => $race): ?>
                    <tr>
                        <td><?=$race['nombre']?></td>
                        <td>
                            <a href="#" class="btn btn-outline-primary" title="Actualizar" data-toggle="modal" data-target="#upRace<?=$key?>"><i class="fas fa-user-edit"></i></a>
                            <!-- Actualizar Carrera -->
                            <div class="modal fade" id="upRace<?=$key?>" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Actualizar Carrera</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="<?=$post['upRace']?>?page=<?=$page?>" method="POST">
                                        <input type="hidden" name="idRace" value="<?=$key?>" />
                                        <div class="modal-body">
                                            <div class="form-row">
                                                <div class="col">
                                                    <label for="txtUpName<?=$key?>">Nombre de Carrera<i style="color: red;">*</i></label>
                                                    <input type="text" name="txtName" value="<?=$race['nombre']?>" id="txtUpName<?=$key?>" class="form-control" placeholder="Nombre" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-outline-primary">Actualizar</button>
                                        </div>
                                    </form>
                                  </div>
                                </div>
                            </div>

                            <a href="#" urlDel="<?=$get['delRace']?>?page=<?=$page?>&id=<?=$key?>" onclick="onClickDelete(this)" class="btn btn-outline-danger" title="Eliminar"><i class="fas fa-trash-alt"></i></a>
                            <a href="#" class="btn btn-outline-secondary" title="Ver" data-toggle="modal" data-target="#showRace<?=$key?>"><i class="fas fa-eye"></i></a>
                            <!-- Modal Mostrar Carrera -->
                            <div class="modal fade" id="showRace<?=$key?>" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Visualizando Carrera</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-row">
                                            <div class="col">
                                                <label for="txtRaceName<?=$key?>">Nombre de Carrera</label>
                                                <input type="text" value="<?=$race['nombre']?>" id="txtRaceName<?=$key?>" class="form-control" placeholder="Nombre" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Nueva Carrera -->
<div class="modal fade" id="addRace" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nuevo Usuario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
          <form action="<?=$post['addRace']?>?page=<?=$page?>" method="POST">
            <div class="modal-body">
                <div class="form-row">
                    <div class="col">
                        <label for="txtAddNameRace">Nombre de Carrera</label>
                        <input type="text" name="txtName" id="txtAddNameRace" class="form-control" placeholder="Nombre de Carrera" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-outline-primary">Guardar</button>
            </div>
        </form>
      </div>
    </div>
</div>

<?php include $partial['messages']; ?>