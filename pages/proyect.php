<?php
    $proyects = $instance['proyect']->getProyects();
    $gProyects = $instance['proyect']->getProyectGroups();
    $locations = $instance['location']->getLocations();
    $categories = $instance['category']->getCategories();
    $students = $instance['student']->getStudents();
    $studentsINproyect = $instance['student']->getStudentInProyect();
    $teachers = $instance['teacher']->getTeachers();
    
    $idsArray = [];// alamacena el id de los estudiantes
?>

<h1 class="mt-4">Proyectos</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Proyectos</li>
</ol>

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Proyectos
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <caption>
                    <button data-toggle="modal" data-tagert="#addProyect" class="btn btn-outline-primary" title="Agregar Proyecto" data-toggle="modal" data-target="#addProyect">
                        <i class="fas fa-plus"></i>
                    </button>
                </caption>
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Integrantes</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Integrantes</th>
                        <th>Opciones</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php foreach($gProyects as $key => $gProyect): ?>
                    <tr>
                        <td><img src="<?=$img['proyect']?>" width="40"/> <?=$gProyect['nombreProyecto']?></td>
                        <td><?=$gProyect['descripcionProyecto']?></td>
                        <td>
                            <?php foreach($proyects as $keyProyect => $proyect): ?>
                                <?php if($proyect['idProyecto'] == $gProyect['idProyecto']): ?>
                                    <?php $idsArray[$keyProyect] = $proyect['idEstudiante']; ?>
                                    <?=$proyect['estudiante']?>
                                    <br />
                                <?php endif;?>
                            <?php endforeach;?>
                        </td>
                        <td>
                            <a href="#" class="btn btn-outline-primary" title="Actualizar" data-toggle="modal" data-target="#upProyect<?=$key?>"><i class="fas fa-user-edit"></i></a>
                            <!-- Actualizar Proyecto -->
                            <div class="modal fade" id="upProyect<?=$key?>" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Actualizar Proyecto</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                      <form action="<?=$post['upProyect']?>?page=<?=$page?>" method="POST">
                                        <input type="hidden" name="idProyect" value="<?=$gProyect['idProyectoEstudiante']?>" />
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="txtUpname<?=$key?>">Nombre</label>
                                                <input type="text" name="txtName" value="<?=$gProyect['nombreProyecto']?>" id="txtUpname<?=$key?>" class="form-control" placeholder="Nombre" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtUpDescription<?=$key?>">Descripción</label>
                                                <textarea rows="2" name="txtDescription" id="txtAddDescription<?=$key?>" class="form-control" placeholder="Descripción de poyecto" required><?=$gProyect['descripcionProyecto']?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtUpPass">Categoría</label>
                                                <select class="form-control" name="txtCategory">
                                                    <?php foreach($categories as $keyCategory => $category): ?>
                                                        <?php if($gProyect['nombreCategoria'] == $category['nombre']): ?>
                                                            <option value="<?=$keyCategory?>" selected=""><?=$category['nombre']?></option>
                                                        <?php else: ?>
                                                            <option value="<?=$keyCategory?>"><?=$category['nombre']?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtUpPass">Ubicación</label>
                                                <select class="form-control" name="txtLocation" required>
                                                    <?php foreach($locations as $keyLocation => $location): ?>
                                                        <?php if($gProyect['idUbicacion'] == $keyLocation): ?>
                                                            <option value="<?=$keyLocation?>" selected><?=$location['lugar'] . ', ' . $location['centro'] . ', ' . $location['facultad'] . ', ' . $location['escuela']?></option>
                                                        <?php else: ?>
                                                            <option value="<?=$keyLocation?>"><?=$location['lugar'] . ', ' . $location['centro'] . ', ' . $location['facultad'] . ', ' . $location['escuela']?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="txtUpStudent">Estudiantes</label>
                                                    <select class="students" style="width: 100%;" name="txtStudent[]" multiple="multiple">
                                                        <?php foreach($students as $keyStudent => $student):?>
                                                            <?php if(in_array($keyStudent, $idsArray)): ?>
                                                                <option value="<?=$keyStudent?>" selected><?=$student['nombre'] . ' ' . $student['apellido'] . ' - ' . $student['matricula']?></option>
                                                            <?php else:?>
                                                                <option value="<?=$keyStudent?>"><?=$student['nombre'] . ' ' . $student['apellido'] . ' - ' . $student['matricula']?></option>
                                                            <?php endif;?>
                                                        <?php endforeach;?>
                                                    </select>
                                                    <select  style="width: 100%;" name="txtOldStudent[]" multiple="multiple" hidden>
                                                        <?php foreach($students as $keyStudent => $student):?>
                                                            <?php if(in_array($keyStudent, $idsArray)): ?>
                                                                <option value="<?=$keyStudent?>" selected><?=$student['nombre'] . ' ' . $student['apellido'] . ' - ' . $student['matricula']?></option>
                                                            <?php else:?>
                                                                <option value="<?=$keyStudent?>"><?=$student['nombre'] . ' ' . $student['apellido'] . ' - ' . $student['matricula']?></option>
                                                            <?php endif;?>
                                                        <?php endforeach;?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="txtUpStudent">Maestro</label>
                                                    <select class="teachers" style="width: 100%;" name="txtTeacher[]" multiple="multiple" required>
                                                        <?php foreach($teachers as $keyTeacher => $teacher):?>
                                                            <?php if($keyTeacher == $gProyect['idProfesor']): ?>
                                                                <option value="<?=$keyTeacher?>" selected><?=$teacher['nombre'] . ' ' . $teacher['apellido']?></option>
                                                            <?php else: ?>
                                                                <option value="<?=$keyTeacher?>"><?=$teacher['nombre'] . ' ' . $teacher['apellido']?></option>
                                                            <?php endif;?>
                                                        <?php endforeach;?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-outline-primary">Actualizar</button>
                                        </div>
                                      </form>
                                  </div>
                                </div>
                            </div>
                            
                            <a href="#" urlDel="<?=$get['delProyect']?>?page=<?=$page?>&id=<?=$key?>" onclick="onClickDelete(this)" class="btn btn-outline-danger" title="Eliminar"><i class="fas fa-trash-alt"></i></a>

                            <a href="#" class="btn btn-outline-secondary" title="Ver" data-toggle="modal" data-target="#showProyect<?=$key?>"><i class="fas fa-eye"></i></a>
                            <!-- Mostrar Proyecto -->
                            <div class="modal fade" id="showProyect<?=$key?>" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Visualizando Proyecto</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="txtShowName<?=$key?>">Nombre</label>
                                            <input type="text" value="<?=$gProyect['nombreProyecto']?>" id="txtShowName<?=$key?>" class="form-control" placeholder="Nombre" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtShowDescription<?=$key?>">Descripción</label>
                                            <textarea rows="3" id="txtShowDescription<?=$key?>" class="form-control" placeholder="Descripción de poyecto" readonly><?=$gProyect['descripcionProyecto']?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtUpPass">Categoría</label>
                                            <select class="form-control" disabled>
                                                <?php foreach($categories as $key => $category): ?>
                                                    <?php if($gProyect['nombreCategoria'] == $category['nombre']): ?>
                                                        <option value="<?=$key?>" selected=""><?=$category['nombre']?></option>
                                                    <?php else: ?>
                                                        <option value="<?=$key?>"><?=$category['nombre']?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtUpPass">Ubicación</label>
                                            <select class="form-control" disabled>
                                                <?php foreach($locations as $keyLocation => $location): ?>
                                                    <?php if($gProyect['idUbicacion'] == $keyLocation): ?>
                                                        <option value="<?=$keyLocation?>" selected><?=$location['lugar'] . ', ' . $location['centro'] . ', ' . $location['facultad'] . ', ' . $location['escuela']?></option>
                                                    <?php else: ?>
                                                        <option value="<?=$keyLocation?>"><?=$location['lugar'] . ', ' . $location['centro'] . ', ' . $location['facultad'] . ', ' . $location['escuela']?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtUpStudent">Estudiantes</label>
                                            <select onchange="onBoard(this)" class="students" style="width: 100%;" name="txtStudent[]" multiple="multiple" disabled>
                                                <?php foreach($students as $keyStudent => $student):?>
                                                    <?php if(in_array($keyStudent, $idsArray)): ?>
                                                        <option value="<?=$keyStudent?>" selected><?=$student['nombre'] . ' ' . $student['apellido'] . ' - ' . $student['matricula']?></option>
                                                    <?php else:?>
                                                        <option value="<?=$keyStudent?>"><?=$student['nombre'] . ' ' . $student['apellido'] . ' - ' . $student['matricula']?></option>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtUpStudent">Maestro</label>
                                            <select class="teachers" style="width: 100%;" multiple="multiple" disabled>
                                                <?php foreach($teachers as $keyTeacher => $teacher):?>
                                                    <?php if($keyTeacher == $gProyect['idProfesor']): ?>
                                                        <option value="<?=$keyTeacher?>" selected><?=$teacher['nombre'] . ' ' . $teacher['apellido']?></option>
                                                    <?php else: ?>
                                                        <option value="<?=$keyTeacher?>"><?=$teacher['nombre'] . ' ' . $teacher['apellido']?></option>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    
                    <?php 
                    $idsArray=[];
                    
                    ?>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Nuevo Proyecto -->
<div class="modal fade" id="addProyect" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nuevo Proyecto</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
          <form action="<?=$post['addProyect']?>?page=<?=$page?>" method="POST">
            <div class="modal-body" >
                <div class="form-group">
                    <label for="txtaddname">Nombre</label>
                    <input type="text" name="txtName" id="txtaddname" class="form-control" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <label for="txtAddDescription">Descripción</label>
                    <textarea rows="2" name="txtDescription" id="txtAddDescription" class="form-control" placeholder="Descripción de rpoyecto" required></textarea>
                </div>
                <div class="form-group">
                    <label for="txtAddPass">Categoría</label>
                    <select class="form-control" name="txtCategory">
                        <?php foreach($categories as $keyCategory => $category): ?>
                        <option value="<?=$keyCategory?>"><?=$category['nombre']?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="txtAddPass">Ubicación</label>
                    <select class="form-control" name="txtLocation" required>
                        <?php foreach($locations as $keyLoc => $location): ?>
                        <option value="<?=$keyLoc?>"><?=$location['lugar'] . ', ' . $location['centro'] . ', ' . $location['facultad'] . ', ' . $location['escuela']?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="txtAddStudent">Estudiantes</label>
                        <select onchange="onBoard(this)" class="students" style="width: 100%;" name="txtStudent[]" multiple="multiple" required>
                            <?php foreach($students as $key => $student):?>
                            <option value="<?=$key?>">
                                <?php if(inArray($student['nombre'] . ' ' . $student['apellido'], $studentsINproyect)): ?>
                                    <?=$student['nombre'] . ' ' . $student['apellido'] . ' - ' . $student['matricula'] . ' ► En Proyecto'?>
                                <?php else: ?>
                                    <?=$student['nombre'] . ' ' . $student['apellido'] . ' - ' . $student['matricula']?>
                                <?php endif;?>
                            </option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="txtAddStudent">Maestro</label>
                        <select class="teachers" style="width: 100%;" name="txtTeacher[]" multiple="multiple" required>
                            <?php foreach($teachers as $key => $teacher):?>
                            <option value="<?=$key?>"><?=$teacher['nombre'] . ' ' . $teacher['apellido']?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-outline-primary">Guardar</button>
            </div>
        </form>
      </div>
    </div>
</div>

<script>
    function onClickSearchEstudent(value){
        console.log($(value).val())
    }
    
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.students').select2({
            maximumSelectionLength: 4
        });
        $('.teachers').select2({
            maximumSelectionLength: 1
        });
    })
</script>

<?php include $partial['messages']; ?>