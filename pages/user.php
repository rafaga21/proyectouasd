<?php
    $users = $instance['user']->getUsers();
?>

<?php if($instance['user']->Admin()): ?>
<h1 class="mt-4">Usuarios</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">usuarios</li>
</ol>

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Usuarios
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <caption>
                    <button class="btn btn-outline-primary" title="Agregar Usuario" data-toggle="modal" data-target="#addUser">
                        <i class="fas fa-plus"></i>
                    </button>
                </caption>
                <thead>
                    <tr>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Usuario</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Usuario</th>
                        <th>Opciones</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php foreach($users as $key => $value): ?>
                    <tr style="<?=$key == $instance['user']->getUserId() ? 'background-color: #D8EEDA;' : ''?>">
                        <td style="background-color: white;">
                            <?php if($instance['user']->isAdmin($key)):?>
                                <img src="<?=$img['admin']?>" width="50" />
                            <?php else:?>
                                <img src="<?=$img['user']?>" width="50" />
                            <?php endif;?>
                        </td>
                        <td><?=$value['nombre']?></td>
                        <td><?=$value['apellido']?></td>
                        <td style="color: white;background-color: #73B87A;"><?=$value['usuario']?></td>
                        <td>
                            <?php if(!$instance['user']->isAdmin($key) || $key == $instance['user']->getUserId()): ?>
                                <a userId="<?=$key?>" name="<?=$value['nombre']?>" lastname="<?=$value['apellido']?>" username="<?=$value['usuario']?>" href="#" onclick="updateUser(this)" class="btn btn-outline-primary" title="Actualizar" data-toggle="modal" data-target="#upUser"><i class="fas fa-user-edit"></i></a>
                                <?php if(!$instance['user']->isAdmin($key)): ?>
                                    <a href="#" urlDel="<?=$get['delUser']?>?page=<?=$page?>&id=<?=$key?>" onclick="onClickDelete(this)" class="btn btn-outline-danger" title="Eliminar"><i class="fas fa-trash-alt"></i></a>
                                <?php endif; ?>
                            <?php endif; ?>
                            <a href="#" class="btn btn-outline-secondary" title="Ver" data-toggle="modal" data-target="#showUser<?=$key?>"><i class="fas fa-eye"></i></a>
                            
                            <!-- Mostrar Usuario -->
                            <div class="modal fade" id="showUser<?=$key?>" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Visualizando Usuario</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card mb-3">
                                            <?php if($instance['user']->isAdmin($key)):?>
                                                <img class="card-img-top" src="<?=$img['admin']?>" />
                                            <?php else:?>
                                                <img class="card-img-top" src="<?=$img['user']?>" />
                                            <?php endif;?>
                                            <div class="card-body">
                                                <h5 class="card-title"><?=$value['nombre'] . ' ' . $value['apellido']?></h5>
                                                <p class="card-text">Usuario: [<?=$value['usuario']?>]</p>
                                                <?php if($instance['user']->isAdmin($key)):?>
                                                    <p class="card-text"><small class="text-muted">Administrador</small></p>
                                                <?php else:?>
                                                    <p class="card-text"><small class="text-muted">Usuario</small></p>
                                                <?php endif;?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php else: ?>
    <script>
        window.location = "./?page=home";
    </script>
<?php endif;?>
    
<!-- Nuevo Usuario -->
<div class="modal fade" id="addUser" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nuevo Usuario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="<?=$post['addUser']?>?page=<?=$page?>" method="POST">
            <div class="modal-body">
                <div class="form-row">
                    <div class="col">
                        <label for="txtaddname">Nombre</label>
                        <input type="text" name="name" id="txtaddname" class="form-control" placeholder="Nombre" required>
                    </div>
                    <div class="col">
                        <label for="txtAddLastName">Apellido</label>
                        <input type="text" name="lastname" id="txtAddLastName" class="form-control" placeholder="Apellido" required>
                    </div>
                </div>
                <br/>
                <div class="form-group">
                    <label for="txtAddUser">Usuario</label>
                    <input type="text" name="username" id="txtAddUser" class="form-control" placeholder="Usuario" required>
                    <small id="emailHelp" class="form-text text-muted">nombre de usuario.</small>
                </div>
                <div class="form-group">
                    <label for="txtAddPass">Contraseña</label>
                    <input type="password" class="form-control" name="password" id="txtAddPass" placeholder="Contraseña" minlength="6" required>
                </div>
                <div class="form-group">
                    <label for="txtrAddPass">Repetir Contraseña</label>
                    <input type="password" class="form-control" name="password1" id="txtrAddPass" placeholder="Contraseña" minlength="6" required>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" value="" name="txtadmin" id="admin" />
                    <label class="form-check-label" for="admin">Administrador</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-outline-primary">Guardar</button>
            </div>
        </form>
      </div>
    </div>
</div>

<!-- Actualizar Usuario -->
<div class="modal fade" id="upUser" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Actualizar Usuario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="<?=$post['upUser']?>?page=<?=$page?>" method="POST">
            <input type="hidden" name="userId" id="userId" />
            <input type="hidden" name="lastusername" id="lastusername" />
            <div class="modal-body">
                <div class="form-row">
                    <div class="col">
                        <label for="txtUpName">Nombre <i style="color: red;">*</i></label>
                        <input type="text" name="name" id="txtUpName" class="form-control" placeholder="Nombre" required>
                    </div>
                    <div class="col">
                        <label for="txtUpLastName">Apellido <i style="color: red;">*</i></label>
                        <input type="text" name="lastname" id="txtUpLastName" class="form-control" placeholder="Apellido" required>
                    </div>
                </div>
                <br/>
                <div class="form-group">
                    <label for="txtUpUser">Usuario <i style="color: red;">*</i></label>
                    <input type="text" name="username" id="txtUpUser" class="form-control" placeholder="Usuario" required>
                    <small id="emailHelp" class="form-text text-muted">nombre de usuario.</small>
                </div>
                <?php if(!$instance['user']->Admin()): ?>
                <div class="form-group">
                    <label for="txtUpPass">Contraseña Anterior <i style="color: red;">*</i></label>
                    <input type="password" name="password" class="form-control" id="txtUpPass" placeholder="Contraseña" minlength="4" required>
                </div>
                <?php endif;?>
                <div class="form-group">
                    <label for="txtUprPass">Nueva Contraseña</label>
                    <input type="password" class="form-control" name="password1" id="txtUprPass" placeholder="Contraseña" minlength="6">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-outline-primary">Actualizar</button>
            </div>
        </form>
      </div>
    </div>
</div>

<?php include $partial['messages']; ?>