<script>
    function showMessage(title, icon='success'){
        const Toast = Swal.mixin({
            toast: true,
            position: 'bottom-right',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Toast.fire({
          icon: icon,
          title: title
        })
    }
</script>

<?php if(isset($_GET['del'])): ?>
    <script>
        showMessage('Registro eliminado.')
    </script>
<?php elseif(isset($_GET['errUser'])): ?>
    <script>
        Swal.fire(
            'Nuevo Usuario',
            'Las claves no coinciden.',
            'error'
        )
    </script>
<?php elseif(isset($_GET['errAdd'])): ?>
    <script>
        Swal.fire(
            'Error',
            'No se pudo agregar el registro.',
            'error'
        )
    </script>
<?php elseif(isset($_GET['add'])): ?>
    <script>
        showMessage('Datos Registrados!')
    </script>
<?php elseif(isset($_GET['up'])): ?>
    <script>
        showMessage('Datos Actualizados!')
    </script>
<?php elseif(isset($_GET['errUp'])): ?>
    <script>
        Swal.fire(
            'Actualizar Datos',
            'Datos Erroneos.',
            'error'
        )
    </script>
<?php elseif(isset($_GET['errDel'])): ?>
    <script>
        Swal.fire(
            'Eliminar Registro',
            'Error al Eliminar Registro.',
            'error'
        )
    </script>
<?php endif; ?>