<?php

include './../../controller/connection/Connection.php';
include './../../controller/teacher/Teacher.php';
include './../../controller/student/Student.php';
include './../../controller/student/StudentRace.php';
include './../../controller/user/User.php';
include './../../controller/proyect/Proyect.php';
include './../../controller/proyect/ProyectEstudent.php';
include './../../controller/category/Category.php';
include './../../controller/location/Country.php';
include './../../controller/location/Location.php';

$user = new User();
$student = new Student();
$race = new Race();
$teacher = new Teacher();
$category = new Category();
$proyect = new ProyectEstudent();
$location = new Location();