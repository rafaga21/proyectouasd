<?php

function getFiles($nameDir){
    $dir = opendir($nameDir);
    $files = "{";
    while($file = readdir($dir)){
        if(strlen($file) > 2){
            $file = explode('.', $file);
            $fileName = $file[0];
            $ext = $file[count($file)-1];
            $path = "./$nameDir/$fileName.$ext";
            $files .= "\"{$fileName}\":\"{$path}\",";
        }
    }
    $files = substr($files, 0, (strlen($files)-1));
    $files .= "}";
    $files = json_decode(trim($files), true);
    $files = $files == NULL ? [] : $files;
    return $files;
}

function showConsole($title, $message){
    $title = strtoupper($title);
    $message = strtolower($message);
    echo "<script>console.log('$title: $message')</script>";
}

function inArray(string $text, array $data){
    $answer = false;
    foreach($data as $dat){
        foreach($dat as $value){
            if($text == $value){
                $answer = true;
                break;
            }
        }
        if($answer){
            break;
        }
    }
    return $answer;
}
