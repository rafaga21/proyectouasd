<?php

include './../../partial/requestLibrary.php';

$page = isset($_GET['page']) ? $_GET['page'] : '';
$update = '';

if($user->Auth()){
    if(isset($_POST['txtName']) && isset($_POST['txtLastName']) && isset($_POST['txtCarrera']) && isset($_POST['txtMatricula'])){
        $id = $_POST['idStudent'];
        $name = $_POST['txtName'];
        $lastName = $_POST['txtLastName'];
        $idCarrera = $_POST['txtCarrera'];
        $matricula = $_POST['txtMatricula'];
        if(!empty($name) && !empty($lastName) && !empty($id)){
            $student->updateStudent($id, $name, $lastName, $idCarrera, $matricula);
            $update = 'up';
        }else{
            $update = 'err';
        }
    }
}

header("Location: ./../../?page=$page&$update");