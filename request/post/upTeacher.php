<?php

include './../../partial/requestLibrary.php';

$page = isset($_GET['page']) ? $_GET['page'] : '';
$update = '';

if($user->Auth()){
    if(isset($_POST['txtName']) && isset($_POST['txtLastName']) && isset($_POST['idTeacher'])){
        $id = $_POST['idTeacher'];
        $name = $_POST['txtName'];
        $lastName = $_POST['txtLastName'];
        if(!empty($name) && !empty($lastName) && !empty($id)){
            $teacher->updateTeacher($id, $name, $lastName);
            $update = 'up';
        }else{
            $update = 'err';
        }
    }
}

header("Location: ./../../?page=$page&$update");