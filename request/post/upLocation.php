<?php

include '../../partial/requestLibrary.php';

$update = '';

if(isset($_GET['page']) && $user->Auth()){
    $page = $_GET['page'];
    $idLocation = $_GET['id'];
    $place = $_POST['txtPlace'];
    $center = $_POST['txtCenter'];
    $faculty = $_POST['txtFaculty'];
    $school = $_POST['txtSchool'];
    $city = $_POST['txtCity'];
    $idCountry = $_POST['txtCountry'];
    $location->updateLocation($idLocation, $idCountry, $place, $center, $faculty, $school, $city);
    $update = "?page=$page&up";
}

header("Location: ./../../$update");

