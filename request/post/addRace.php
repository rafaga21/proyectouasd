<?php

include './../../partial/requestLibrary.php';

$page = isset($_GET['page']) ? $_GET['page'] : '';
$delete = '';

if($user->Auth()){
    if(isset($_POST['txtName'])){
        $name = $_POST['txtName'];
        if(!empty($name)){
            $race->addRace($name);
            $delete = 'add';
        }else{
            $delete = 'err';
        }
    }
}

header("Location: ./../../?page=$page&$delete");