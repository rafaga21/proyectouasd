<?php

include './../../partial/requestLibrary.php';

$page = isset($_GET['page']) ? $_GET['page'] : '';
$add = '';

if($user->Auth()){
    if(isset($_POST['txtName']) && isset($_POST['txtLastName'])){
        $name = $_POST['txtName'];
        $lastName = $_POST['txtLastName'];
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        if(!empty($name) && !empty($lastName)){
            $teacher->addTeacher($name, $lastName);
            $add = 'add';
        }else{
            $add = 'errAdd';
        }
    }
}

header("Location: ./../../?page=$page&$add");