<?php

include '../../partial/requestLibrary.php';

$add = '';

if(isset($_GET['page']) && $user->Auth()){
    $page = $_GET['page'];
    $place = $_POST['txtPlace'];
    $center = $_POST['txtCenter'];
    $faculty = $_POST['txtFaculty'];
    $school = $_POST['txtSchool'];
    $city = $_POST['txtCity'];
    $idCountry = $_POST['txtCountry'];
    $location->addLocation($idCountry, $place, $center, $faculty, $school, $city);
    $add = "?page=$page&add";
}

header("Location: ./../../$add");