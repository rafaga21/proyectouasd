<?php

include '../../partial/requestLibrary.php';

$update = '';

if(isset($_GET['page']) && $user->Auth()){
    $page = $_GET['page'];
    $studentsID = $_POST['txtStudent'];
    $oldStudentsID = $_POST['txtOldStudent'];
    $name = $_POST['txtName'];
    $description = $_POST['txtDescription'];
    $categoryID = $_POST['txtCategory'];
    $teacherID = $_POST['txtTeacher'];
    $locationID = $_POST['txtLocation'];
    $idUser = $user->getUserId();
    $idProyectStudent = $_POST['idProyect'];
    $proyect->updateProyect($idProyectStudent, $name, $description, $locationID, $categoryID, $teacherID[0], $idUser);
    $proyect->newStudentInProyect($idProyectStudent, $studentsID, $oldStudentsID);
    $update = "?page=$page&up";
}

header("Location: ./../../$update");
