<?php

include '../../partial/requestLibrary.php';

$page = isset($_GET['page']) ? $_GET['page'] : '';
$update = '';

if($user->Auth()){
    if(isset($_POST['username']) && (isset($_POST['password']) || isset($_POST['password1']))){
        $update = 'up';
        if($user->Admin()){
            $user->updateUser($_POST['userId'], $_POST['name'], $_POST['lastname'], $_POST['username'], $_POST['password1']);
        }else if($user->isUser($_POST['lastusername'], isset($_POST['password']) ? $_POST['password'] : $_POST['password1'])){
            $user->updateUser($_POST['userId'], $_POST['name'], $_POST['lastname'], $_POST['username'], $_POST['password1']);
        }else{
            $update = 'errUp';
        }
    }
}

header("Location: ./../../?page=$page&$update");