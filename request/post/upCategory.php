<?php

include './../../partial/requestLibrary.php';

$page = isset($_GET['page']) ? $_GET['page'] : '';
$update = '';

if($user->Auth()){
    if(isset($_POST['txtName']) && isset($_POST['idCategory'])){
        $id = $_POST['idCategory'];
        $name = $_POST['txtName'];
        if(!empty($name) && !empty($id)){
            $category->updateCategory($id, $name);
            $update = 'up';
        }else{
            $update = 'err';
        }
    }
}

header("Location: ./../../?page=$page&$update");

