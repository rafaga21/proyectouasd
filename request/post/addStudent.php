<?php

include './../../partial/requestLibrary.php';

$page = isset($_GET['page']) ? $_GET['page'] : '';
$add = '';

if($user->Auth()){
    if(isset($_POST['txtName']) && isset($_POST['txtLastName']) && isset($_POST['txtCarrera']) && isset($_POST['txtMatricula'])){
        $name = $_POST['txtName'];
        $lastName = $_POST['txtLastName'];
        $idCarrera = $_POST['txtCarrera'];
        $matricula = $_POST['txtMatricula'];
        if(!empty($name) && !empty($lastName) && !empty($idCarrera) && !empty($matricula)){
            $student->addStudent($name, $lastName, $idCarrera, $matricula);
            $add = 'add';
        }else{
            $add = 'err';
        }
    }
}

header("Location: ./../../?page=$page&$add");

