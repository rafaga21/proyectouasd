<?php

include '../../partial/requestLibrary.php';

$page = isset($_GET['page']) ? $_GET['page'] : '';
$add = '';

if($user->Auth()){
    if(isset($_POST['username']) && isset($_POST['password'])){
        if($_POST['password'] == $_POST['password1']){
            $user->addUser($_POST['name'], $_POST['lastname'], $_POST['username'], $_POST['password'], isset($_POST['txtadmin']) ? 1 : 0);
            $add = 'add';
        }else{
            $add = 'errUser';
        }
    }
}

header("Location: ./../../?page=$page&$add");