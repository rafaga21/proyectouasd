<?php


include './../../partial/requestLibrary.php';

if(isset($_GET['id']) && $user->Auth()){
    $id = $_GET['id'];
    $page = isset($_GET['page']) ? $_GET['page'] : '';
    if(!empty($id)){
        $race->deleteRace($id);
        header("Location: ./../../?page=$page&del");
    }else{
        header("Location: ./../../?page=$page&errDel");
    }
}else{
    header('Location: ./../../');
}
