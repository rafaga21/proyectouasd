<?php

include './../../partial/requestLibrary.php';

$delete = '';
if(isset($_GET['id']) && $user->Auth()){
    $id = $_GET['id'];
    $page = isset($_GET['page']) ? $_GET['page'] : '';
    if(!empty($id)){
        $category->deleteCategory($id);
        $delete = "?page=$page&del";
    }else{
        $delete = "?page=$page&errDel";
    }
}

header("Location: ./../../$delete");

