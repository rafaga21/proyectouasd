<?php

include './../../partial/requestLibrary.php';

$page = isset($_GET['page']) ? $_GET['page'] : '';
$delete = '';

if($user->Auth()){
    if(isset($_GET['id'])){
        $student->deleteStudent($_GET['id']);
        $delete = 'del';
    }
}

header("Location: ./../../?page=$page&$delete");