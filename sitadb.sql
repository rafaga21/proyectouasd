-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-05-2021 a las 20:11:47
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sitadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera`
--

CREATE TABLE `carrera` (
  `idCarrera` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `eliminado` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `carrera`
--

INSERT INTO `carrera` (`idCarrera`, `nombre`, `eliminado`) VALUES
(1, 'Lic. en Informática', 0),
(2, 'Ing. de Software', 0),
(3, 'Administración de Empresas', 0),
(4, 'Ing. Civil', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `idCategoria` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `eliminado` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`idCategoria`, `nombre`, `eliminado`) VALUES
(1, 'Tesis', 0),
(2, 'Monográfico', 0),
(3, 'Tesina', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante`
--

CREATE TABLE `estudiante` (
  `idEstudiante` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `idCarrera` int(11) NOT NULL,
  `matricula` varchar(10) NOT NULL,
  `eliminado` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `idPais` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `eliminado` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`idPais`, `nombre`, `eliminado`) VALUES
(1, 'República Dominicana', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `idProfesor` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `eliminado` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `idProyecto` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `idUbicacion` int(11) NOT NULL,
  `idCategoria` int(11) NOT NULL,
  `eliminado` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectoestudiante`
--

CREATE TABLE `proyectoestudiante` (
  `idProyectoEstudiante` int(11) NOT NULL,
  `idProyecto` int(11) NOT NULL,
  `idEstudiante` int(11) NOT NULL,
  `idProfesor` int(11) NOT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `eliminado` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicacion`
--

CREATE TABLE `ubicacion` (
  `idUbicacion` int(11) NOT NULL,
  `lugar` varchar(70) NOT NULL,
  `centro` varchar(30) NOT NULL,
  `facultad` varchar(45) NOT NULL,
  `escuela` varchar(45) NOT NULL,
  `ciudad` varchar(45) NOT NULL,
  `idPais` int(11) NOT NULL,
  `eliminado` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `nombreUsuario` varchar(30) NOT NULL,
  `clave` varchar(40) NOT NULL,
  `eliminado` tinyint(1) DEFAULT 0,
  `admin` tinyint(1) DEFAULT 0,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `nombre`, `apellido`, `nombreUsuario`, `clave`, `eliminado`, `admin`, `fecha`) VALUES
(1, 'Rafael', 'Minaya Beltrán', 'Rafaga21', '4e46088ec803ef3a0ee9bf53f518cd42', 0, 1, '2021-05-15 17:02:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE `visitas` (
  `idVisita` int(10) NOT NULL,
  `cantidadVisita` int(10) DEFAULT 1,
  `idUsuario` int(10) NOT NULL,
  `ipVisita` varchar(20) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_carrera`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_carrera` (
`idCarrera` int(11)
,`nombre` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_estudiante`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_estudiante` (
`idEstudiante` int(11)
,`nombre` varchar(30)
,`apellido` varchar(30)
,`carrera` varchar(50)
,`matricula` varchar(10)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_estudiantesenproyecto`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_estudiantesenproyecto` (
`idEstudiante` int(11)
,`estudiante` varchar(61)
,`matricula` varchar(10)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_profesor`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_profesor` (
`idProfesor` int(11)
,`nombre` varchar(45)
,`apellido` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_proyectoestudiante`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_proyectoestudiante` (
`idProyectoEstudiante` int(11)
,`idProyecto` int(11)
,`idEstudiante` int(11)
,`idProfesor` int(11)
,`idUsuario` int(11)
,`idUbicacion` int(11)
,`nombreProyecto` varchar(100)
,`descripcionProyecto` varchar(200)
,`lugarUbicacion` varchar(70)
,`centroUbicacion` varchar(30)
,`facultadUbicacion` varchar(45)
,`escuelaUbicacion` varchar(45)
,`ciudadUbicacion` varchar(45)
,`nombrePais` varchar(45)
,`nombreCategoria` varchar(45)
,`estudiante` varchar(61)
,`carreraEstudiante` varchar(50)
,`matriculaEstudiante` varchar(10)
,`profesor` varchar(91)
,`empleado` varchar(61)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_ubicacion`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_ubicacion` (
`idUbicacion` int(11)
,`idPais` int(11)
,`lugar` varchar(70)
,`centro` varchar(30)
,`facultad` varchar(45)
,`escuela` varchar(45)
,`ciudad` varchar(45)
,`pais` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_usuario`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_usuario` (
`idUsuario` int(10)
,`nombre` varchar(30)
,`apellido` varchar(30)
,`usuario` varchar(30)
,`clave` varchar(40)
,`admin` tinyint(1)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_visitas`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_visitas` (
`idVisita` int(10)
,`ipVisita` varchar(20)
,`cantidadVisita` int(10)
,`idUsuario` int(10)
,`usuario` varchar(61)
,`nombreUsuario` varchar(30)
,`fecha` timestamp
);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_carrera`
--
DROP TABLE IF EXISTS `vista_carrera`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_carrera`  AS SELECT `carrera`.`idCarrera` AS `idCarrera`, `carrera`.`nombre` AS `nombre` FROM `carrera` WHERE `carrera`.`eliminado` = 0 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_estudiante`
--
DROP TABLE IF EXISTS `vista_estudiante`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_estudiante`  AS SELECT `e`.`idEstudiante` AS `idEstudiante`, `e`.`nombre` AS `nombre`, `e`.`apellido` AS `apellido`, `c`.`nombre` AS `carrera`, `e`.`matricula` AS `matricula` FROM (`estudiante` `e` join `carrera` `c` on(`e`.`idCarrera` = `c`.`idCarrera`)) WHERE `e`.`eliminado` = 0 AND `c`.`eliminado` = 0 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_estudiantesenproyecto`
--
DROP TABLE IF EXISTS `vista_estudiantesenproyecto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_estudiantesenproyecto`  AS SELECT `e`.`idEstudiante` AS `idEstudiante`, concat(`e`.`nombre`,' ',`e`.`apellido`) AS `estudiante`, `e`.`matricula` AS `matricula` FROM (`estudiante` `e` join `proyectoestudiante` `pe` on(`e`.`idEstudiante` = `pe`.`idEstudiante` and `pe`.`eliminado` = 0)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_profesor`
--
DROP TABLE IF EXISTS `vista_profesor`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_profesor`  AS SELECT `profesor`.`idProfesor` AS `idProfesor`, `profesor`.`nombre` AS `nombre`, `profesor`.`apellido` AS `apellido` FROM `profesor` WHERE `profesor`.`eliminado` = 0 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_proyectoestudiante`
--
DROP TABLE IF EXISTS `vista_proyectoestudiante`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_proyectoestudiante`  AS SELECT `pe`.`idProyectoEstudiante` AS `idProyectoEstudiante`, `pe`.`idProyecto` AS `idProyecto`, `pe`.`idEstudiante` AS `idEstudiante`, `pe`.`idProfesor` AS `idProfesor`, `pe`.`idUsuario` AS `idUsuario`, `p`.`idUbicacion` AS `idUbicacion`, `p`.`nombre` AS `nombreProyecto`, `p`.`descripcion` AS `descripcionProyecto`, `u`.`lugar` AS `lugarUbicacion`, `u`.`centro` AS `centroUbicacion`, `u`.`facultad` AS `facultadUbicacion`, `u`.`escuela` AS `escuelaUbicacion`, `u`.`ciudad` AS `ciudadUbicacion`, `ps`.`nombre` AS `nombrePais`, `c`.`nombre` AS `nombreCategoria`, concat(`e`.`nombre`,' ',`e`.`apellido`) AS `estudiante`, `crr`.`nombre` AS `carreraEstudiante`, `e`.`matricula` AS `matriculaEstudiante`, concat(`prfs`.`nombre`,' ',`prfs`.`apellido`) AS `profesor`, concat(`usr`.`nombre`,' ',`usr`.`apellido`) AS `empleado` FROM ((((((((`proyectoestudiante` `pe` join `proyecto` `p` on(`p`.`idProyecto` = `pe`.`idProyecto`)) join `ubicacion` `u` on(`p`.`idUbicacion` = `u`.`idUbicacion`)) join `pais` `ps` on(`u`.`idPais` = `ps`.`idPais`)) join `categoria` `c` on(`c`.`idCategoria` = `p`.`idCategoria`)) join `estudiante` `e` on(`pe`.`idEstudiante` = `e`.`idEstudiante` and `e`.`eliminado` = 0)) join `carrera` `crr` on(`crr`.`idCarrera` = `e`.`idCarrera`)) join `profesor` `prfs` on(`prfs`.`idProfesor` = `pe`.`idProfesor`)) join `usuario` `usr` on(`usr`.`idUsuario` = `pe`.`idUsuario`)) WHERE `pe`.`eliminado` = 0 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_ubicacion`
--
DROP TABLE IF EXISTS `vista_ubicacion`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_ubicacion`  AS SELECT `u`.`idUbicacion` AS `idUbicacion`, `u`.`idPais` AS `idPais`, `u`.`lugar` AS `lugar`, `u`.`centro` AS `centro`, `u`.`facultad` AS `facultad`, `u`.`escuela` AS `escuela`, `u`.`ciudad` AS `ciudad`, `p`.`nombre` AS `pais` FROM (`ubicacion` `u` join `pais` `p` on(`u`.`idPais` = `p`.`idPais`)) WHERE `p`.`eliminado` = 0 AND `u`.`eliminado` = 0 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_usuario`
--
DROP TABLE IF EXISTS `vista_usuario`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_usuario`  AS SELECT `usuario`.`idUsuario` AS `idUsuario`, `usuario`.`nombre` AS `nombre`, `usuario`.`apellido` AS `apellido`, `usuario`.`nombreUsuario` AS `usuario`, `usuario`.`clave` AS `clave`, `usuario`.`admin` AS `admin` FROM `usuario` WHERE `usuario`.`eliminado` = 0 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_visitas`
--
DROP TABLE IF EXISTS `vista_visitas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_visitas`  AS SELECT `v`.`idVisita` AS `idVisita`, `v`.`ipVisita` AS `ipVisita`, `v`.`cantidadVisita` AS `cantidadVisita`, `u`.`idUsuario` AS `idUsuario`, concat(`u`.`nombre`,' ',`u`.`apellido`) AS `usuario`, `u`.`nombreUsuario` AS `nombreUsuario`, `v`.`fecha` AS `fecha` FROM (`visitas` `v` join `usuario` `u` on(`v`.`idUsuario` = `u`.`idUsuario`)) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carrera`
--
ALTER TABLE `carrera`
  ADD PRIMARY KEY (`idCarrera`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD PRIMARY KEY (`idEstudiante`),
  ADD UNIQUE KEY `matricula` (`matricula`),
  ADD KEY `idCarrera` (`idCarrera`),
  ADD KEY `idCarrera_2` (`idCarrera`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`idPais`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`idProfesor`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`idProyecto`),
  ADD KEY `idUbicacion` (`idUbicacion`),
  ADD KEY `idCategoria` (`idCategoria`);

--
-- Indices de la tabla `proyectoestudiante`
--
ALTER TABLE `proyectoestudiante`
  ADD PRIMARY KEY (`idProyectoEstudiante`),
  ADD KEY `idTesis` (`idProyecto`),
  ADD KEY `idEstudiante` (`idEstudiante`),
  ADD KEY `idProfesor` (`idProfesor`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  ADD PRIMARY KEY (`idUbicacion`),
  ADD KEY `idPais` (`idPais`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`idVisita`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carrera`
--
ALTER TABLE `carrera`
  MODIFY `idCarrera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `idCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  MODIFY `idEstudiante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `idPais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `idProfesor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `idProyecto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proyectoestudiante`
--
ALTER TABLE `proyectoestudiante`
  MODIFY `idProyectoEstudiante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  MODIFY `idUbicacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `idVisita` int(10) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD CONSTRAINT `idCarrera` FOREIGN KEY (`idCarrera`) REFERENCES `carrera` (`idCarrera`);

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `idCategoria` FOREIGN KEY (`idCategoria`) REFERENCES `categoria` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idUbicacion` FOREIGN KEY (`idUbicacion`) REFERENCES `ubicacion` (`idUbicacion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyectoestudiante`
--
ALTER TABLE `proyectoestudiante`
  ADD CONSTRAINT `idEstudiante` FOREIGN KEY (`idEstudiante`) REFERENCES `estudiante` (`idEstudiante`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idProfesor` FOREIGN KEY (`idProfesor`) REFERENCES `profesor` (`idProfesor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idTesis` FOREIGN KEY (`idProyecto`) REFERENCES `proyecto` (`idProyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  ADD CONSTRAINT `idPais` FOREIGN KEY (`idPais`) REFERENCES `pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
