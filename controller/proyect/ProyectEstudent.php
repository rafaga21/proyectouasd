<?php

class ProyectEstudent extends Proyect{
    private $table;
    private $vista;
                
    public function __construct() {
        parent::__construct();
        $this->table = 'proyectoestudiante';
        $this->vista = "vista_$this->table";
    }
    
    public function newProyect(string $name, string $description, int $idLocation, int $idCategory, int $idStudent, int $idTeacher, int $idUser){
        parent::addProyect($name, $description, $idLocation, $idCategory);
        $sql = "INSERT INTO $this->table(idProyecto, idEstudiante, idProfesor, idUsuario) VALUES ($this->lastID, $idStudent, $idTeacher, $idUser)";
        parent::execNoQuery($sql);
    }
    
    public function getProyects(){
        $sql = "SELECT * FROM $this->vista order by idProyecto";
        return parent::getData($sql, 'idProyectoEstudiante');
    }
    
    public function getProyectGroups(){
        $sql = "SELECT * FROM $this->vista GROUP BY idProyecto";
        return parent::getData($sql, 'idProyectoEstudiante');
    }
    
    public function deleteProyect(int $id){
        $idProyect = parent::getValue($this->table, 'idProyecto', 'idProyectoEstudiante', $id);
        $sql = "UPDATE $this->table SET eliminado=true where idProyecto=$idProyect";
        //echo "$sql<br/>";
        parent::execNoQuery($sql);
    }
    
    private function deleteStudentOfProyect(int $idProyect, int $idStudent){
        $sql = "UPDATE $this->table SET eliminado=TRUE WHERE idProyecto=$idProyect AND idEstudiante=$idStudent";
        parent::execNoQuery($sql);
    }
    
    private function addStudentInProyect(int $idStudent, int $idProyect, int $idTeacher, int $idUser){
        $sql = "INSERT INTO $this->table(idProyecto, idEstudiante, idProfesor, idUsuario) VALUES ($idProyect, $idStudent, $idTeacher, $idUser)";
        parent::execNoQuery($sql);
    }
    
    public function newStudentInProyect(int $idProyectStudent, array $newStudents, array $oldStudents){
        $idProyect = parent::getValue($this->table, 'idProyecto', 'idProyectoEstudiante', $idProyectStudent);
        $idTeacher = parent::getValue($this->table, 'idProfesor', 'idProyectoEstudiante', $idProyectStudent);
        $idUser = parent::getValue($this->table, 'idUsuario', 'idProyectoEstudiante', $idProyectStudent);
        
        foreach($oldStudents as $oldId){// Eliminando estudiantes viejos
            if(!in_array($oldId, $newStudents)){
                $this->deleteStudentOfProyect($idProyect, $oldId);
            }
        }
        
        foreach($newStudents as $newId){// Agregando nuevos estudiantes
            if(!in_array($newId, $oldStudents)){
                $this->addStudentInProyect($newId, $idProyect, $idTeacher, $idUser);
            }
        }
    }
    
    public function updateProyect(int $idProyectStudent, string $name, string $description, int $idLocation, int $idCategory, int $idTeacher, int $idUser){
        $idProyect = parent::getValue($this->vista, 'idProyecto', 'idProyectoEstudiante', $idProyectStudent);
        parent::upProyect($idProyect, $name, $description, $idLocation, $idCategory);
        $sql = "UPDATE $this->table SET idProfesor=$idTeacher, idUsuario=$idUser WHERE idProyectoEstudiante = $idProyectStudent AND NOT eliminado";
        parent::execNoQuery($sql);
    }
}