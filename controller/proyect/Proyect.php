<?php

class Proyect extends Connection{
    private $table;
    private $vista;
    
    public function __construct() {
        parent::__construct();
        $this->table = 'proyecto';
        $this->vista = "vista_$this->table";
    }
    
    protected function addProyect(string $name, string $description, int $idLocation, int $idCategory){
        if($this->name != $name && $this->description != $description && $this->idLocation != $idLocation && $this->idCategory != $idCategory){
            $this->name = $name;
            $this->description = $description;
            $this->idLocation = $idLocation;
            $this->idCategory = $idCategory;
            $sql = "INSERT INTO $this->table(nombre, descripcion, idUbicacion, idCategoria) VALUES('$name', '$description', $idLocation, $idCategory)";
            parent::execNoQuery($sql);
            $this->idProyect = $this->lastID;
        }else{
            $this->lastID = $this->idProyect;
        }
    }
    
    protected function upProyect(int $idProyect, string $name, string $description, int $idLocation, int $idCategory){
        $sql = "UPDATE $this->table SET nombre='$name', descripcion='$description', idUbicacion=$idLocation, idCategoria=$idCategory WHERE idProyecto=$idProyect";
        parent::execNoQuery($sql);
    }
}

