<?php

class Visit extends Connection{
    private $ip;
    private $id;
    private $table;
    private $view;
    
    public function __construct() {
        parent::__construct();
        $this->table = 'visitas';
        $this->id = isset($_SESSION['id'])? $_SESSION['id']: '';
        $this->view = "vista_$this->table";
        $this->ip = $_SERVER['REMOTE_ADDR'];
    }
    
    private function isVisit(){
        $sql = "SELECT * FROM $this->table WHERE ipVisita='$this->ip' AND idUsuario=$this->id";
        return count(parent::getData($sql, 'idVisita')) > 0;
    }
    
    public function newVisit(){
        $sql = "INSERT INTO $this->table(ipVisita,idUsuario) VALUES('$this->ip', $this->id)";
        if(isset($_SESSION['id'])){
            if(!$this->isVisit()){
                parent::execNoQuery($sql);
            }else{
                $this->updateVisit();
            }
        }
    }
    
    private function updateVisit(){
        $sql = "UPDATE $this->table SET cantidadVisita=(cantidadVisita+1), fecha=NOW() WHERE ipVisita='$this->ip' AND idUsuario=$this->id";
        parent::execNoQuery($sql);
    }
    
    public function getAllVisits(): array{
        $sql = "SELECT * FROM $this->view";
        return parent::getData($sql, 'idVisita');
    }
    
    public function getTotalVisits(): int{
        $visits = $this->getAllVisits();
        $cant = 0;
        foreach($visits as $key => $visit){
            $cant += $visit['cantidadVisita'];
        }
        
        return $cant;
    }
    
    public function getVisits(): int{
        $sql = "SELECT idUsuario, sum(cantidadVisita) as cantidad FROM $this->view WHERE idUsuario=$this->id";
        return parent::getData($sql, 'idUsuario')[$this->id]['cantidad'];
    }
}
