<?php

class Teacher extends Connection{
    private $table;
    private $view;
    
    public function __construct(){
        parent::__construct();
        $this->table = 'profesor';
        $this->view = "vista_$this->table";
    }
    
    public function addTeacher(string $name, string $lastName){
        $sql = "INSERT INTO $this->table(nombre, apellido) VALUES('$name','$lastName')";
        parent::execNoQuery($sql);
    }
    
    public function getTeachers(){
        $sql = "SELECT * FROM $this->view";
        return parent::getData($sql, 'idProfesor');
    }
    
    public function updateTeacher(int $idTeacher, string $name, string $lastName){
        $sql = "UPDATE $this->table SET nombre='$name', apellido='$lastName' WHERE idProfesor = $idTeacher";
        parent::execNoQuery($sql);
    }
    
    public function deleteTeacher(int $idTeacher){
        $sql = "UPDATE $this->table SET eliminado=TRUE WHERE idProfesor = $idTeacher";
        parent::execNoQuery($sql);
    }
}