<?php

class Location extends Country{
    public function __construct() {
        parent::__construct();
        $this->table = 'ubicacion';
        $this->vista = "vista_$this->table";
    }
    
    public function addLocation(int $idCountry, string $place, string $center, string $faculty, string $school, string $city){
        $sql = "INSERT INTO $this->table(lugar,centro,facultad,escuela,ciudad,idPais) VALUES('$place','$center','$faculty','$school','$city',$idCountry)";
        parent::execNoQuery($sql);
    }
    
    public function updateLocation(int $idLocation, int $idCountry, string $place, string $center, string $faculty, string $school, string $city){
        $sql = "UPDATE $this->table SET lugar='$place', centro='$center', facultad='$faculty', escuela='$school', ciudad='$city', idPais=$idCountry WHERE idUbicacion=$idLocation";
        parent::execNoQuery($sql);
    }
    
    public function getLocations(){
        $sql = "SELECT * FROM $this->vista";
        return parent::getData($sql, 'idUbicacion');
    }
    
    public function deleteLocation(int $idLocation){
        $sql = "UPDATE $this->table SET eliminado=TRUE WHERE idUbicacion=$idLocation";
        parent::execNoQuery($sql);
    }
}

