<?php

class Country extends Connection{
    private $table;
    
    public function __construct() {
        parent::__construct();
        $this->table = 'pais';
    }
    
    protected function addCountry(string $name){
        $sql = "INSERT INTO $this->table(name) VALUES('$name')";
        parent::execNoQuery($sql);
    }
    
    protected function upCountry(int $idCountry, string $name){
        $sql = "UPDATE $this->table SET name='$name' WHERE idCountry=$idCountry";
        parent::execNoQuery($sql);
    }
    
    public function getCountries(){
        $sql = "SELECT idPais, nombre FROM $this->table WHERE NOT eliminado";
        return parent::getData($sql, 'idPais');
    }
}

