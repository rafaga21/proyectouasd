<?php

class User extends Connection{
    private $table;
    private $view;
    
    public function __construct() {
        parent::__construct();
        $this->table = 'usuario';
        $this->view = "vista_$this->table";
    }
    
    public function addUser(string $name, string $lastName, string $username, string $password, $admin){
        $sql = "INSERT INTO $this->table(nombre,apellido,nombreUsuario,clave,admin) VALUES('$name', '$lastName', '$username', MD5('$password'), $admin)";
        parent::execNoQuery($sql);
    }
    
    public function updateUser(int $idUser, string $name, string $lastName, string $username, string $password){
        if(!empty($password)){
            $sql = "UPDATE $this->table SET nombre='$name',apellido='$lastName',nombreUsuario='$username',clave=MD5('$password'),admin='$admin' WHERE idUsuario=$idUser";
        }else{
            $sql = "UPDATE $this->table SET nombre='$name',apellido='$lastName',nombreUsuario='$username' WHERE idUsuario=$idUser";
        }
        parent::execNoQuery($sql);
    }
    
    public function isUser(string $userName, string $password){
        $sql = "SELECT * FROM $this->view WHERE usuario = '$userName' AND clave = MD5('$password')";
        $datos = parent::getData($sql, "idUsuario");
        $access = count($datos) == 1;
        if($access){
            foreach ($datos as $key => $value){
                $_SESSION['id'] = $value['idUsuario'];
                $_SESSION['usuario'] = $value['usuario'];
                $_SESSION['admin'] = $value['admin'];
                $_SESSION['nombre'] = $value['nombre'] . ' ' . $value['apellido'];
                $_SESSION['page'] = 'home';
                break;
            }
        }
        
        return $access;
    }
    
    public function getUsers(){
        $sql = "SELECT * FROM $this->view ORDER BY admin DESC";
        return parent::getData($sql, "idUsuario");
    }
    
    public function deleteUser(int $idUser){
        $sql = "UPDATE $this->table SET eliminado=TRUE WHERE id$this->table = $idUser";
        parent::execNoQuery($sql);
    }
    
    public function Auth(){
        return isset($_SESSION['usuario']) && isset($_SESSION['nombre']) && isset($_SESSION['id']);
    }
    
    public function Admin(){
        return isset($_SESSION['admin']) ? $_SESSION['admin'] : false;
    }
    
    public function isAdmin(int $idUser): bool{
        $sql = "SELECT * FROM vista_usuario WHERE idUsuario=$idUser AND admin";
        return count(parent::getData($sql, 'idUsuario')) > 0;
    }
    
    public function getName(){
        return isset($_SESSION['nombre']) ? $_SESSION['nombre'] : '';
    }
    
    public function getUserId(){
        return isset($_SESSION['id']) ? $_SESSION['id'] : -1;
    }
    
    public function getUserName(){
        return isset($_SESSION['usuario']) ? $_SESSION['usuario'] : '';
    }
    
    public function logout(){
        session_destroy();
        unset($_SESSION);
    }
}