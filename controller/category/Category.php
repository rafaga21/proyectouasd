<?php

class Category extends Connection{
    public function __construct() {
        parent::__construct();
        $this->table = 'categoria';
    }
    
    public function addCategory(string $nameCategory){
        $sql = "INSERT INTO $this->table(nombre) VALUES('$nameCategory')";
        parent::execNoQuery($sql);
    }
    
    public function getCategories(){
        $sql = "SELECT idCategoria, nombre FROM $this->table WHERE NOT eliminado";
        return parent::getData($sql, 'idCategoria');
    }
    
    public function updateCategory(int $idCategory, string $name){
        $sql = "UPDATE $this->table SET nombre='$name' WHERE idCategoria=$idCategory";
        parent::execNoQuery($sql);
    }
    
    public function deleteCategory(int $idCategory){
        $sql = "UPDATE $this->table SET eliminado=true WHERE idCategoria=$idCategory";
        parent::execNoQuery($sql);
    }
}

