<?php

class Student extends Connection{
    private $table;
    private $view;
    
    public function __construct(){
        parent::__construct();
        $this->table = 'estudiante';
        $this->view = "vista_$this->table";
    }
    
    public function addStudent(string $name, string $lastName, int $idRace, string $enrollment){
        $sql = "INSERT INTO $this->table(nombre, apellido, idCarrera, matricula) VALUES('$name','$lastName', $idRace, '$enrollment')";
        parent::execNoQuery($sql);
    }
    
    public function getStudents(){
        $sql = "SELECT * FROM $this->view";
        return parent::getData($sql, 'idEstudiante');
    }
    
    public function getStudentInProyect(): array{
        $sql = "SELECT * FROM vista_estudiantesenproyecto";
        return parent::getData($sql, 'idEstudiante');
    }
    
    public function isStudentInProyect(int $idStudent): bool{
        $sql = "SELECT * FROM vista_estudiantesenproyecto WHERE idEstudiante=$idStudent";
        return count(parent::getData($sql, 'idEstudiante')) > 0;
    }


    public function updateStudent(int $idStudent, string $name, string $lastName, int $idRace, string $enrollment){
        $sql = "UPDATE $this->table SET nombre='$name', apellido='$lastName', idCarrera=$idRace, matricula='$enrollment' WHERE idEstudiante = $idStudent";
        parent::execNoQuery($sql);
    }
    
    public function deleteStudent(int $idStudent){
        $sql = "UPDATE $this->table SET eliminado=TRUE WHERE idEstudiante = $idStudent";
        parent::execNoQuery($sql);
    }
}