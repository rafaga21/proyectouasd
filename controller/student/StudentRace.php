<?php

class Race extends Connection{
    private $table;
    private $view;
    
    public function __construct() {
        parent::__construct();
        $this->table = 'carrera';
        $this->view = "vista_$this->table";
    }
    
    public function addRace(string $name){
        $sql = "INSERT INTO $this->table(nombre) VALUES('$name')";
        parent::execNoQuery($sql);
    }
    
    public function deleteRace(int $idRace){
        $sql = "UPDATE $this->table SET eliminado=TRUE WHERE idCarrera=$idRace";
        parent::execNoQuery($sql);
    }
    
    public function updateRace(int $idRace, string $name){
        $sql = "UPDATE $this->table SET nombre='$name' WHERE idCarrera=$idRace";
        parent::execNoQuery($sql);
    }
    
    public function getRaces(): array{
        $sql = "SELECT * FROM $this->view";
        return parent::getData($sql, 'idCarrera');
    }
}